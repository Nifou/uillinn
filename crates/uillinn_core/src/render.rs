//! Define how an application or component is rendered
use crate::prelude::*;

/// Defines how an application or component is transformed into an independent string
/// (or list of files) usable everywhere
///
/// # Example of an **incomplete** SVG render (just to illustrate some concepts, don't use it seriously)
///
/// ```rust
/// use uillinn_core::prelude::*;
///
/// struct SvgRender;
///
/// impl Render for SvgRender {
///     type Result = String;
///
///     fn render_app(&mut self, app: &App) -> Self::Result {
///         // Recursive function to walk along the tree of components
///         fn render_to_string(area: Option<&Area>, component: &Component) -> String {
///             if let Some(area) = area {
///                 match component.kind {
///                     ComponentKind::Rectangle(ref def) => format!(
///                         r#"<rect x="{}" y="{}" width="{}" height="{}" color="{}"></rect>"#,
///                         if let StartPx(x_axis) = area.x_axis.default {
///                             x_axis.to_string()
///                         } else {
///                             todo!()
///                         },
///                         if let StartPx(y_axis) = area.y_axis.default {
///                             y_axis.to_string()
///                         } else {
///                             todo!()
///                         },
///                         if let Px(width) = area.width.default {
///                             width.to_string()
///                         } else {
///                             todo!()
///                         },
///                         if let Px(height) = area.height.default {
///                             height.to_string()
///                         } else {
///                             todo!()
///                         },
///                         def.color,
///                     ),
///                     _ => todo!(),
///                 }
///             } else {
///                 // Special case for the root component
///                 return component
///                     .children
///                     .as_ref()
///                     .unwrap()
///                     .iter()
///                     .map(|(a, c)| render_to_string(Some(a), c))
///                     .collect::<String>();
///             }
///         }
///
///         format!("<svg>{}</svg>", render_to_string(None, &app.root))
///     }
/// }
///
/// let my_component = Component::new()
///     .child(Area::new(10, 10, 90, 90), Rectangle::new().color(Blue));
///
/// let my_app = App::new("My test app", "A beautiful app", "en", my_component);
/// assert_eq!(
///     SvgRender.render_app(&my_app),
///     r##"<svg><rect x="10" y="10" width="90" height="90" color="rgb(0,116,217)"></rect></svg>"##.to_string(),
/// );
/// ```
///
/// # List of implemented renders
///
/// - [uillinn_render_web](https://uillinn.gitlab.io/uillinn/uillinn_render_web): Render components to a website (using HTML and CSS)
/// - [uillinn_render_svg](https://uillinn.gitlab.io/uillinn/uillinn_render_svg): Render components to an SVG image
/// - [uillinn_render_desktop](https://uillinn.gitlab.io/uillinn/uillinn_render_desktop): Render components to a windowed desktop software
pub trait Render {
    /// Represents the generated render data
    type Result;

    /// Render an entire application
    fn render_app(&mut self, app: &App) -> Self::Result;
}

#[cfg(test)]
mod tests {
    use super::*;

    struct FakeRender;

    impl Render for FakeRender {
        type Result = String;

        fn render_app(&mut self, _: &App) -> Self::Result {
            "This is a fake render".to_string()
        }
    }

    #[test]
    fn render_app() {
        let root = Component::new();
        let app = App::new("Hello world!", "This is just a basic app", "en", root);

        assert_eq!(
            FakeRender.render_app(&app),
            "This is a fake render".to_string()
        );
    }
}
