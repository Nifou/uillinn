//! Color spaces representation and manipulation
use std::{fmt, num::ParseIntError, str::FromStr};

pub mod convert;

/// Represents a color formatted in a specific format
///
/// It supports:
/// * Hexadecimal (HEX) colors (using the `FromStr` implementation with a string starting with a `#` character)
/// * Red, green, blue (RGB) colors
/// * Red, green, blue, alpha (RGBA) colors
/// * Hue, saturation, lightness (HSL) colors
/// * Hue, saturation, lightness, alpha (HSLA) colors
/// * A bunch of default colors from [https://clrs.cc](https://clrs.cc)
#[derive(Debug, Hash, Copy, Clone, PartialEq, Eq)]
pub enum Color {
    /// <div style="width: 30px; height: 30px; background-color: transparent; border-radius: 7px; border: 2px solid #1a1a1a;"></div>
    Transparent,

    /// <div style="width: 30px; height: 30px; background-color: #001f3f; border-radius: 7px;"></div>
    Navy,

    /// <div style="width: 30px; height: 30px; background-color: #0074d9; border-radius: 7px;"></div>
    Blue,

    /// <div style="width: 30px; height: 30px; background-color: #7fdbff; border-radius: 7px;"></div>
    Aqua,

    /// <div style="width: 30px; height: 30px; background-color: #39cccc; border-radius: 7px;"></div>
    Teal,

    /// <div style="width: 30px; height: 30px; background-color: #b10dc9; border-radius: 7px;"></div>
    Purple,

    /// <div style="width: 30px; height: 30px; background-color: #f012be; border-radius: 7px;"></div>
    Fuchsia,

    /// <div style="width: 30px; height: 30px; background-color: #85144b; border-radius: 7px;"></div>
    Maroon,

    /// <div style="width: 30px; height: 30px; background-color: #ff4136; border-radius: 7px;"></div>
    Red,

    /// <div style="width: 30px; height: 30px; background-color: #ff851b; border-radius: 7px;"></div>
    Orange,

    /// <div style="width: 30px; height: 30px; background-color: #ffdc00; border-radius: 7px;"></div>
    Yellow,

    /// <div style="width: 30px; height: 30px; background-color: #3d9970; border-radius: 7px;"></div>
    Olive,

    /// <div style="width: 30px; height: 30px; background-color: #2ecc40; border-radius: 7px;"></div>
    Green,

    /// <div style="width: 30px; height: 30px; background-color: #01ff70; border-radius: 7px;"></div>
    Lime,

    /// <div style="width: 30px; height: 30px; background-color: #000000; border-radius: 7px;"></div>
    Black,

    /// <div style="width: 30px; height: 30px; background-color: #aaaaaa; border-radius: 7px;"></div>
    Gray,

    /// <div style="width: 30px; height: 30px; background-color: #dddddd; border-radius: 7px;"></div>
    Silver,

    /// <div style="width: 30px; height: 30px; background-color: #ffffff; border-radius: 7px; border: 2px solid #1a1a1a;"></div>
    White,

    /// Specify colors using [RGB (Red Green Blue) color model](https://www.w3schools.com/colors/colors_rgb.asp)
    ///
    /// # Examples
    ///
    /// <div style="display: flex;">
    ///     <div style="width: 30px; height: 30px; background-color: rgb(234, 196, 53); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgb(234, 196, 53)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: rgb(52, 89, 149); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgb(52, 89, 149)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: rgb(3, 206, 164); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgb(3, 206, 164)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: rgb(251, 77, 61); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgb(251, 77, 61)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: rgb(202, 21, 81); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgb(202, 21, 81)</code>
    /// </div>
    Rgb(u8, u8, u8),

    /// Specify colors using RGBA (Red Green Blue Alpha) color model (same as above but with an alpha channel defined using a percentage)
    ///
    /// # Examples
    ///
    /// <div style="display: flex;">
    ///     <div style="width: 30px; height: 30px; background-color: rgba(234, 196, 53, 1); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgba(234, 196, 53, 100)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: rgba(52, 89, 149, 0.75); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgba(52, 89, 149, 75)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: rgba(3, 206, 164, 0.5); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgba(3, 206, 164, 50)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: rgba(251, 77, 61, 0.25); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgba(251, 77, 61, 25)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: rgba(202, 21, 81, 0); border-radius: 7px; border: 2px solid #1a1a1a;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Rgba(202, 21, 81, 0)</code>
    /// </div>
    Rgba(u8, u8, u8, u8),

    /// Specify colors using [HSL (Hue Saturation Lightness) color model](https://www.w3schools.com/colors/colors_hsl.asp)
    ///
    /// # Examples
    ///
    /// <div style="display: flex;">
    ///     <div style="width: 30px; height: 30px; background-color: hsl(245, 89%, 15%); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsl(245, 89, 15)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: hsl(46, 19%, 40%); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsl(46, 19, 40)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: hsl(34, 99%, 68%); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsl(34, 99, 68)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: hsl(352, 93%, 53%); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsl(352, 93, 53)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: hsl(334, 91%, 40%); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsl(334, 91, 40)</code>
    /// </div>
    Hsl(u16, u8, u8),

    /// Specify colors using HSLA (Hue Saturation Lightness Alpha) color model (same as above but with an alpha channel defined using a percentage)
    ///
    /// # Examples
    ///
    /// <div style="display: flex;">
    ///     <div style="width: 30px; height: 30px; background-color: hsla(245, 89%, 15%, 1); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsla(245, 89, 15, 100)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: hsla(46, 19%, 40%, 0.75); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsla(46, 19, 40, 75)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: hsla(34, 99%, 68%, 0.5); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsla(34, 99, 68, 50)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: hsla(352, 93%, 53%, 0.25); border-radius: 7px;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsla(352, 93, 53, 25)</code>
    /// </div>
    ///
    /// <div style="display: flex; margin-top: 20px;">
    ///     <div style="width: 30px; height: 30px; background-color: hsla(334, 91%, 40%, 0); border-radius: 7px; border: 2px solid #1a1a1a;"></div>
    ///     <code style="margin-left: 10px; align-self: center;">Hsla(334, 91, 40, 0)</code>
    /// </div>
    Hsla(u16, u8, u8, u8),
    // TODO: Gradients
}

impl Color {
    /// If the [`Color`] is a default color returns a `rgb` color value, returns the old color
    /// otherwise
    fn default_color_to_rgb(&self) -> Color {
        match self {
            Self::Navy => Color::Rgb(0, 31, 63),
            Self::Blue => Color::Rgb(0, 116, 217),
            Self::Aqua => Color::Rgb(127, 219, 255),
            Self::Teal => Color::Rgb(57, 204, 204),
            Self::Purple => Color::Rgb(177, 13, 201),
            Self::Fuchsia => Color::Rgb(240, 18, 190),
            Self::Maroon => Color::Rgb(133, 20, 75),
            Self::Red => Color::Rgb(255, 65, 54),
            Self::Orange => Color::Rgb(255, 133, 27),
            Self::Yellow => Color::Rgb(255, 220, 0),
            Self::Olive => Color::Rgb(61, 153, 112),
            Self::Green => Color::Rgb(46, 204, 64),
            Self::Lime => Color::Rgb(1, 255, 112),
            Self::Black => Color::Rgb(0, 0, 0),
            Self::Gray => Color::Rgb(170, 170, 170),
            Self::Silver => Color::Rgb(221, 221, 221),
            Self::White => Color::Rgb(255, 255, 255),
            other => *other,
        }
    }

    /// Darken a color using a percentage (100% -> black, 0% -> original color)
    ///
    /// # Example
    ///
    /// ```rust
    /// use uillinn_core::prelude::*;
    ///
    /// let original_color = Lime;
    /// let color_darken_0 = original_color.darken(0);
    /// let color_darken_10 = original_color.darken(10);
    /// let color_darken_20 = original_color.darken(20);
    /// let color_darken_30 = original_color.darken(30);
    /// let color_darken_40 = original_color.darken(40);
    /// let color_darken_50 = original_color.darken(50);
    /// let color_darken_60 = original_color.darken(60);
    /// let color_darken_70 = original_color.darken(70);
    /// let color_darken_80 = original_color.darken(80);
    /// let color_darken_90 = original_color.darken(90);
    ///
    /// assert_eq!(color_darken_0, Rgb(1, 255, 112));
    /// assert_eq!(color_darken_10, Rgb(0, 231, 101));
    /// assert_eq!(color_darken_20, Rgb(0, 205, 90));
    /// assert_eq!(color_darken_30, Rgb(0, 180, 79));
    /// assert_eq!(color_darken_40, Rgb(0, 154, 67));
    /// assert_eq!(color_darken_50, Rgb(0, 129, 56));
    /// assert_eq!(color_darken_60, Rgb(0, 104, 45));
    /// assert_eq!(color_darken_70, Rgb(0, 78, 34));
    /// assert_eq!(color_darken_80, Rgb(0, 53, 23));
    /// assert_eq!(color_darken_90, Rgb(0, 27, 12));
    /// ```
    ///
    /// <div style="display: flex; margin-top: 20px; flex-wrap: wrap; gap: 10px; justify-content: center;">
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 255, 111); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 230, 99); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 204, 88); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 179, 77); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 153, 66); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 128, 55); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 102, 44); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 77, 33); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 51, 22); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 26, 11); border-radius: 7px;"></div>
    /// </div>
    pub fn darken(&self, percent_amount: u8) -> Self {
        assert!(
            percent_amount < 100,
            "The amount to darken must be less than 100"
        );

        match self {
            Self::Transparent => Self::Transparent,
            Self::Hsla(h, s, l, a) => Self::Hsla(
                *h,
                *s,
                (*l as f32 - (percent_amount as f32 * *l as f32) / 100.).round() as u8,
                *a,
            ),
            Self::Hsl(h, s, l) => Self::Hsl(
                *h,
                *s,
                (*l as f32 - (percent_amount as f32 * *l as f32) / 100.).round() as u8,
            ),
            Self::Rgba(red, green, blue, alpha) => {
                let darken_color = {
                    let (h, s, mut l) = convert::rgb_to_hsl(*red, *green, *blue);
                    l -= (percent_amount as f32 * (100. - l as f32)) / 100.;

                    convert::hsl_to_rgb(h, s, l)
                };

                Self::Rgba(darken_color.0, darken_color.1, darken_color.2, *alpha)
            }
            other => {
                if let Color::Rgb(r, g, b) = other.default_color_to_rgb() {
                    let darken_color = {
                        let (h, s, mut l) = convert::rgb_to_hsl(r, g, b);
                        l -= (percent_amount as f32 * (100. - l as f32)) / 100.;

                        convert::hsl_to_rgb(h, s, l)
                    };

                    Self::Rgb(darken_color.0, darken_color.1, darken_color.2)
                } else {
                    unreachable!();
                }
            }
        }
    }

    /// Lighten a color using a percentage (100% -> black, 0% -> original color)
    ///
    /// # Example
    ///
    /// ```rust
    /// use uillinn_core::prelude::*;
    ///
    /// let original_color = Navy;
    /// let color_lighten_0 = original_color.lighten(0);
    /// let color_lighten_10 = original_color.lighten(10);
    /// let color_lighten_20 = original_color.lighten(20);
    /// let color_lighten_30 = original_color.lighten(30);
    /// let color_lighten_40 = original_color.lighten(40);
    /// let color_lighten_50 = original_color.lighten(50);
    /// let color_lighten_60 = original_color.lighten(60);
    /// let color_lighten_70 = original_color.lighten(70);
    /// let color_lighten_80 = original_color.lighten(80);
    /// let color_lighten_90 = original_color.lighten(90);
    ///
    /// assert_eq!(color_lighten_0, Rgb(0, 31, 63));
    /// assert_eq!(color_lighten_10, Rgb(0, 53, 108));
    /// assert_eq!(color_lighten_20, Rgb(0, 75, 152));
    /// assert_eq!(color_lighten_30, Rgb(0, 97, 197));
    /// assert_eq!(color_lighten_40, Rgb(0, 119, 242));
    /// assert_eq!(color_lighten_50, Rgb(32, 141, 255));
    /// assert_eq!(color_lighten_60, Rgb(76, 164, 255));
    /// assert_eq!(color_lighten_70, Rgb(121, 187, 255));
    /// assert_eq!(color_lighten_80, Rgb(166, 210, 255));
    /// assert_eq!(color_lighten_90, Rgb(210, 232, 255));
    /// ```
    ///
    /// <div style="display: flex; margin-top: 20px; flex-wrap: wrap; gap: 10px; justify-content: center;">
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 31, 63); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 53, 108); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 75, 152); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 97, 197); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(0, 119, 242); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(31, 141, 255); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(76, 164, 255); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(121, 187, 255); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(166, 210, 255); border-radius: 7px;"></div>
    ///     <div style="width: 50px; height: 50px; background-color: rgb(210, 232, 255); border-radius: 7px;"></div>
    /// </div>
    pub fn lighten(&self, percent_amount: u8) -> Self {
        assert!(
            percent_amount < 100,
            "The amount to lighten must be less than 100"
        );

        match self {
            Self::Transparent => Self::Transparent,
            Self::Hsla(h, s, l, a) => Self::Hsla(
                *h,
                *s,
                (*l as f32 + (percent_amount as f32 * (100. - *l as f32)) / 100.).round() as u8,
                *a,
            ),
            Self::Hsl(h, s, l) => Self::Hsl(
                *h,
                *s,
                (*l as f32 + (percent_amount as f32 * (100. - *l as f32)) / 100.).round() as u8,
            ),
            Self::Rgba(red, green, blue, alpha) => {
                let lighten_color = {
                    let (h, s, mut l) = convert::rgb_to_hsl(*red, *green, *blue);
                    l += (percent_amount as f32 * (100. - l as f32)) / 100.;

                    convert::hsl_to_rgb(h, s, l)
                };

                Self::Rgba(lighten_color.0, lighten_color.1, lighten_color.2, *alpha)
            }
            other => {
                if let Color::Rgb(r, g, b) = other.default_color_to_rgb() {
                    let lighten_color = {
                        let (h, s, mut l) = convert::rgb_to_hsl(r, g, b);
                        l += (percent_amount as f32 * (100. - l as f32)) / 100.;

                        convert::hsl_to_rgb(h, s, l)
                    };

                    Self::Rgb(lighten_color.0, lighten_color.1, lighten_color.2)
                } else {
                    unreachable!();
                }
            }
        }
    }
}

impl fmt::Display for Color {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}",
            match self {
                Self::Transparent => "transparent".to_string(),
                Self::Hsla(h, s, l, a) =>
                    format!("hsla({},{}%,{}%,{})", h, s, l, (*a as f32) / 100.),
                Self::Hsl(h, s, l) => format!("hsl({},{}%,{}%)", h, s, l),
                Self::Rgba(r, g, b, a) => format!("rgba({},{},{},{})", r, g, b, (*a as f32) / 100.),
                other =>
                    if let Color::Rgb(r, g, b) = other.default_color_to_rgb() {
                        format!("rgb({},{},{})", r, g, b)
                    } else {
                        unreachable!()
                    },
            }
        )
    }
}

impl<'a> From<&'a str> for Color {
    fn from(s: &'a str) -> Self {
        s.parse().unwrap()
    }
}

impl FromStr for Color {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "transparent" => Ok(Self::Transparent),
            "navy" => Ok(Self::Navy),
            "blue" => Ok(Self::Blue),
            "aqua" => Ok(Self::Aqua),
            "teal" => Ok(Self::Teal),
            "purple" => Ok(Self::Purple),
            "fuchsia" => Ok(Self::Fuchsia),
            "maroon" => Ok(Self::Maroon),
            "red" => Ok(Self::Red),
            "orange" => Ok(Self::Orange),
            "yellow" => Ok(Self::Yellow),
            "olive" => Ok(Self::Olive),
            "green" => Ok(Self::Green),
            "lime" => Ok(Self::Lime),
            "black" => Ok(Self::Black),
            "gray" => Ok(Self::Gray),
            "silver" => Ok(Self::Silver),
            "white" => Ok(Self::White),
            _ if s.starts_with('#') => {
                let mut val = s.replace('#', "");

                if val.len() == 3 {
                    // Expand 3 digits shortcode
                    val = val
                        .chars()
                        .map(|c| format!("{}{}", c, c))
                        .collect::<String>();
                }

                if val.len() % 2 != 0 {
                    return Err("failed to parse the hexadecimal color");
                }

                let channels = (0..val.len())
                    .step_by(2)
                    .map(|i| u8::from_str_radix(&val[i..i + 2], 16))
                    .collect::<Result<Vec<u8>, ParseIntError>>()
                    .expect("failed to convert the hexadecimal numbers to RGB channels");

                Ok(Self::Rgb(channels[0], channels[1], channels[2]))
            }
            _ if s.starts_with("rgb-") => {
                let mut s = s.split('-');
                s.next(); // Skip `rgb-`

                Ok(Self::Rgb(
                    s.next()
                        .expect("failed to get the red channel")
                        .parse::<u8>()
                        .expect("failed to convert the red channel to a number"),
                    s.next()
                        .expect("failed to get the green channel")
                        .parse::<u8>()
                        .expect("failed to convert the green channel to a number"),
                    s.next()
                        .expect("failed to get the blue channel")
                        .parse::<u8>()
                        .expect("failed to convert the blue channel to a number"),
                ))
            }
            _ if s.starts_with("rgba-") => {
                let mut s = s.split('-');
                s.next(); // Skip `rgba-`

                Ok(Self::Rgba(
                    s.next()
                        .expect("failed to get the red channel")
                        .parse::<u8>()
                        .expect("failed to convert the red channel to a number"),
                    s.next()
                        .expect("failed to get the green channel")
                        .parse::<u8>()
                        .expect("failed to convert the green channel to a number"),
                    s.next()
                        .expect("failed to get the blue channel")
                        .parse::<u8>()
                        .expect("failed to convert the blue channel to a number"),
                    s.next()
                        .expect("failed to get the alpha channel")
                        .parse::<u8>()
                        .expect("failed to convert the alpha channel to a number"),
                ))
            }
            _ if s.starts_with("hsl-") => {
                let mut s = s.split('-');
                s.next(); // Skip `hsl-`

                Ok(Self::Hsl(
                    s.next()
                        .expect("failed to get the hue channel")
                        .parse::<u16>()
                        .expect("failed to convert the red hue to a number"),
                    s.next()
                        .expect("failed to get the saturation channel")
                        .parse::<u8>()
                        .expect("failed to convert the saturation channel to a number"),
                    s.next()
                        .expect("failed to get the lightness channel")
                        .parse::<u8>()
                        .expect("failed to convert the lightness channel to a number"),
                ))
            }
            _ if s.starts_with("hsla-") => {
                let mut s = s.split('-');
                s.next(); // Skip `hsla-`

                Ok(Self::Hsla(
                    s.next()
                        .expect("failed to get the hue channel")
                        .parse::<u16>()
                        .expect("failed to convert the red hue to a number"),
                    s.next()
                        .expect("failed to get the saturation channel")
                        .parse::<u8>()
                        .expect("failed to convert the saturation channel to a number"),
                    s.next()
                        .expect("failed to get the lightness channel")
                        .parse::<u8>()
                        .expect("failed to convert the lightness channel to a number"),
                    s.next()
                        .expect("failed to get the alpha channel")
                        .parse::<u8>()
                        .expect("failed to convert the alpha channel to a number"),
                ))
            }
            _ => Err("failed to parse the string to `Color`"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::prelude::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn parse_hex() {
        let navy = Color::from("#001f3f");
        let blue = "#0074d9".parse::<Color>().unwrap();
        let aqua = "#7fdbff".parse::<Color>().unwrap();
        let teal = "#39cccc".parse::<Color>().unwrap();
        let purple = "#b10dc9".parse::<Color>().unwrap();
        let fuchsia = "#F012BE".parse::<Color>().unwrap();
        let maroon = "#85144b".parse::<Color>().unwrap();
        let red = "#FF4136".parse::<Color>().unwrap();
        let orange = "#FF851B".parse::<Color>().unwrap();
        let yellow = "#FFDC00".parse::<Color>().unwrap();
        let olive = "#3D9970".parse::<Color>().unwrap();
        let green = "#2ECC40".parse::<Color>().unwrap();
        let lime = "#01FF70".parse::<Color>().unwrap();
        let black = "#000".parse::<Color>().unwrap();
        let gray = "#AAA".parse::<Color>().unwrap();
        let silver = "#DDDDDD".parse::<Color>().unwrap();
        let white = "#ffffff".parse::<Color>().unwrap();
        let other_shortcode = "#333".parse::<Color>().unwrap();

        assert_eq!(navy, Color::Navy.default_color_to_rgb());
        assert_eq!(blue, Color::Blue.default_color_to_rgb());
        assert_eq!(aqua, Color::Aqua.default_color_to_rgb());
        assert_eq!(teal, Color::Teal.default_color_to_rgb());
        assert_eq!(purple, Color::Purple.default_color_to_rgb());
        assert_eq!(fuchsia, Color::Fuchsia.default_color_to_rgb());
        assert_eq!(maroon, Color::Maroon.default_color_to_rgb());
        assert_eq!(red, Color::Red.default_color_to_rgb());
        assert_eq!(yellow, Color::Yellow.default_color_to_rgb());
        assert_eq!(orange, Color::Orange.default_color_to_rgb());
        assert_eq!(olive, Color::Olive.default_color_to_rgb());
        assert_eq!(green, Color::Green.default_color_to_rgb());
        assert_eq!(lime, Color::Lime.default_color_to_rgb());
        assert_eq!(black, Color::Black.default_color_to_rgb());
        assert_eq!(gray, Color::Gray.default_color_to_rgb());
        assert_eq!(silver, Color::Silver.default_color_to_rgb());
        assert_eq!(white, Color::White.default_color_to_rgb());
        assert_eq!(other_shortcode, Color::Rgb(51, 51, 51));
    }

    #[test]
    fn color_from_str() {
        let color = Color::from_str("rgb-20-10-30").unwrap();
        assert_eq!(color, Color::Rgb(20, 10, 30));

        let color = Color::from_str("rgba-0-10-0-80").unwrap();
        assert_eq!(color, Color::Rgba(0, 10, 0, 80));

        let color = Color::from_str("hsl-120-81-22").unwrap();
        assert_eq!(color, Color::Hsl(120, 81, 22));

        let color = Color::from_str("hsla-120-81-90-100").unwrap();
        assert_eq!(color, Color::Hsla(120, 81, 90, 100));
    }

    #[test]
    fn darken_test() {
        let original_color = Lime;
        let color_darken_0 = original_color.darken(0);
        let color_darken_10 = original_color.darken(10);

        assert_eq!(color_darken_0, Rgb(1, 255, 112));
        assert_eq!(color_darken_10, Rgb(0, 231, 101));

        let original_color = Rgba(149, 81, 209, 66);
        let color_darken_0 = original_color.darken(0);
        let color_darken_10 = original_color.darken(10);

        assert_eq!(color_darken_0, Rgba(149, 81, 209, 66));
        assert_eq!(color_darken_10, Rgba(138, 64, 204, 66));

        let original_color = Hsl(173, 90, 61);
        let color_darken_0 = original_color.darken(0);
        let color_darken_10 = original_color.darken(10);

        assert_eq!(color_darken_0, Hsl(173, 90, 61));
        assert_eq!(color_darken_10, Hsl(173, 90, 55));

        let original_color = Hsla(173, 90, 61, 55);
        let color_darken_0 = original_color.darken(0);
        let color_darken_50 = original_color.darken(50);

        assert_eq!(color_darken_0, Hsla(173, 90, 61, 55));
        assert_eq!(color_darken_50, Hsla(173, 90, 31, 55));

        let original_color = Transparent;
        let color_darken_0 = original_color.darken(0);
        let color_darken_10 = original_color.darken(10);

        assert_eq!(color_darken_0, Transparent);
        assert_eq!(color_darken_10, Transparent);
    }

    #[test]
    fn lighten_test() {
        let original_color = Navy;
        let color_lighten_0 = original_color.lighten(0);
        let color_lighten_10 = original_color.lighten(10);

        assert_eq!(color_lighten_0, Rgb(0, 31, 63));
        assert_eq!(color_lighten_10, Rgb(0, 53, 108));

        let original_color = Rgba(176, 56, 103, 66);
        let color_lighten_0 = original_color.lighten(0);
        let color_lighten_10 = original_color.lighten(10);

        assert_eq!(color_lighten_0, Rgba(176, 56, 103, 66));
        assert_eq!(color_lighten_10, Rgba(195, 65, 116, 66));

        let original_color = Hsl(78, 35, 14);
        let color_lighten_0 = original_color.lighten(0);
        let color_lighten_10 = original_color.lighten(10);

        assert_eq!(color_lighten_0, Hsl(78, 35, 14));
        assert_eq!(color_lighten_10, Hsl(78, 35, 23));

        let original_color = Hsla(254, 91, 54, 42);
        let color_lighten_0 = original_color.lighten(0);
        let color_lighten_50 = original_color.lighten(50);

        assert_eq!(color_lighten_0, Hsla(254, 91, 54, 42));
        assert_eq!(color_lighten_50, Hsla(254, 91, 77, 42));

        let original_color = Transparent;
        let color_lighten_0 = original_color.lighten(0);
        let color_lighten_10 = original_color.lighten(10);

        assert_eq!(color_lighten_0, Transparent);
        assert_eq!(color_lighten_10, Transparent);
    }

    #[test]
    fn display_color() {
        assert_eq!(Rgb(159, 36, 78).to_string(), "rgb(159,36,78)");
        assert_eq!(Rgba(113, 154, 202, 50).to_string(), "rgba(113,154,202,0.5)");
        assert_eq!(Hsl(88, 85, 51).to_string(), "hsl(88,85%,51%)");
        assert_eq!(Hsla(278, 51, 48, 42).to_string(), "hsla(278,51%,48%,0.42)");
        assert_eq!(Transparent.to_string(), "transparent");
        assert_eq!(Color::from("#2fd16e").to_string(), "rgb(47,209,110)");
    }

    #[test]
    #[should_panic(expected = "The amount to darken must be less than 100")]
    fn panic_when_darken_amount_more_than_100() {
        let original_color = Hsla(254, 91, 54, 42);
        original_color.darken(200);
    }

    #[test]
    #[should_panic(expected = "The amount to lighten must be less than 100")]
    fn panic_when_lighten_amount_more_than_100() {
        let original_color = Hsla(254, 91, 54, 42);
        original_color.lighten(200);
    }
}
