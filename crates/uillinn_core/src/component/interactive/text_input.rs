//! Definition of the `TextInput` component
//!
//! Defaults:
//! - Placeholder: `Enter some text…`
//! - Minimum length: None
//! - Maximum length: None
//! - Validation regular expression: None
//! - Spellcheck enabled: true
use crate::{component::interactive::InteractiveComponent, prelude::*};

/// An input where text can be entered using the keyboard
///
/// Note that like all interactive components, it has no appearance
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct TextInput {
    /// The default text displayed when the input is empty
    ///
    /// Usually used to provide a hint to the user of what can be entered in the field
    pub placeholder: Text,

    /// The minimum length of the text entered
    pub min_len: Option<usize>,

    /// The maximum length of the text entered
    pub max_len: Option<usize>,

    /// A regular expression defining the pattern used to validate the entered text
    pub validation: Option<String>,

    /// Is spell checking enabled?
    pub spell_check: bool,
}

impl TextInput {
    /// Create a new default `TextInput`
    pub fn new() -> Self {
        Self::default()
    }

    /// Change the placeholder text
    pub fn placeholder(mut self, placeholder: Text) -> Self {
        self.placeholder = placeholder;
        self
    }

    /// Change the minimum length of the text entered
    pub fn min_len(mut self, len: usize) -> Self {
        self.min_len = Some(len);
        self
    }

    /// Change the maxiumum length of the text entered
    pub fn max_len(mut self, len: usize) -> Self {
        self.max_len = Some(len);
        self
    }

    /// Change the regular expression defining the pattern used to validate the entered text
    pub fn validation<T: Into<String>>(mut self, val: T) -> Self {
        self.validation = Some(val.into());
        self
    }
}

impl CustomComponent for TextInput {
    fn build(self) -> Component {
        Component {
            name: crate::component::DEFAULT_NAME.to_string(),
            kind: ComponentKind::Interactive(InteractiveComponentKind::TextInput(self)),
            children: None,
        }
    }
}

impl InteractiveComponent for TextInput {}

impl Default for TextInput {
    fn default() -> Self {
        Self {
            placeholder: Text::new("Enter some text…"),
            min_len: None,
            max_len: None,
            validation: None,
            spell_check: true,
        }
    }
}
