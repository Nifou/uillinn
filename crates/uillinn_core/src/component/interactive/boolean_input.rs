//! Definition of the `BooleanInput` component
//!
//! Defaults:
//! - Is checked by default: false
use crate::{component::interactive::InteractiveComponent, prelude::*};

/// An input where text can be entered using the keyboard
///
/// Note that like all interactive components, it has no appearance
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct BooleanInput {
    /// The component displayed when the input equals true
    pub true_case: Box<Component>,

    /// The component displayed when the input equals false
    pub false_case: Box<Component>,

    /// If `true`, the `true_case` component will be displayed by default instead of the
    /// `false_case` component
    pub is_checked_by_default: bool,
}

impl BooleanInput {
    /// Create a new default `BooleanInput`
    pub fn new<T1: CustomComponent, T2: CustomComponent>(true_case: T1, false_case: T2) -> Self {
        Self {
            true_case: Box::new(true_case.build()),
            false_case: Box::new(false_case.build()),
            is_checked_by_default: false,
        }
    }

    /// The input will show the `true_case` component by default
    pub fn checked_by_default(mut self) -> Self {
        self.is_checked_by_default = true;
        self
    }
}

impl CustomComponent for BooleanInput {
    fn build(self) -> Component {
        Component {
            name: crate::component::DEFAULT_NAME.to_string(),
            kind: ComponentKind::Interactive(InteractiveComponentKind::BooleanInput(self)),
            children: None,
        }
    }
}

impl InteractiveComponent for BooleanInput {}
