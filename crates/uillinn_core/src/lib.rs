//! The core crate of Uillinn
//!
//! It provides abstractions used during render
#![doc(html_playground_url = "https://play.rust-lang.org/")]
#![doc(html_logo_url = "https://gitlab.com/uillinn/artwork/-/raw/main/logo/uillinn-logo.svg")]
#![warn(missing_docs)]
pub mod app;
pub mod color;
pub mod component;
pub mod layout;
pub mod render;

/// The `uillinn_core` prelude
pub mod prelude {
    pub use super::app::App;
    pub use super::color::Color::{self, *};
    pub use super::component::{
        Alignment::{self, *},
        AllOrEach::{self, *},
        BooleanInput, Component, ComponentKind, CustomComponent, Ellipse, InteractiveAction,
        InteractiveActionKind::{self, *},
        InteractiveComponentKind, InteractiveGroup,
        IsRequired::{self, *},
        Paragraph, Path, Rectangle, Text, TextInput,
    };
    pub use super::layout::{
        Area,
        Pos::{self, *},
        ScreenBased,
        Size::{self, *},
        CENTER, FULL,
    };
    pub use super::render::Render;
}
