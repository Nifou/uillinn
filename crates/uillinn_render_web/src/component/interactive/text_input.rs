use super::{empty_if_default, format_id};
use uillinn_core::prelude::*;

pub fn render(
    name: Option<&str>,
    is_required: &IsRequired,
    def: &TextInput,
    layout: &str,
    id: Option<String>,
) -> String {
    format!(
        r#"<input{}{} type="text"{}{}{}{} style="{}{}{}{}{}{}{}"{}>"#,
        format_id(&id),
        if let Some(ref name) = name {
            format!(r#" name="{}""#, name)
        } else {
            String::new()
        },
        empty_if_default(
            &def.placeholder.content,
            &String::new(),
            r#" placeholder="{}""#
        ),
        if let Some(ref min_len) = def.min_len {
            format!(r#" minlength="{}""#, min_len)
        } else {
            String::new()
        },
        if let Some(ref max_len) = def.max_len {
            format!(r#" maxlength="{}""#, max_len)
        } else {
            String::new()
        },
        if let Some(ref validation) = def.validation {
            format!(r#" pattern="{}""#, validation)
        } else {
            String::new()
        },
        layout,
        empty_if_default(def.placeholder.align, Left, "text-align:{};"),
        empty_if_default(def.placeholder.color, Black, "color:{};"),
        empty_if_default(
            &def.placeholder.font_family,
            &"sans-serif".into(),
            "font-family:{};"
        ),
        empty_if_default(def.placeholder.font_size, 16, "font-size:{}px;"),
        empty_if_default(def.placeholder.font_weight, 400, "font-weight:{};"),
        empty_if_default(def.placeholder.rotation, 0, "transform:rotate({}deg);"),
        match is_required {
            Required => " required".to_string(),
            NotRequired(default) =>
                if default.is_empty() {
                    "".to_string()
                } else {
                    format!(r#" value="{}""#, default)
                },
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_text_input() {
        let component = TextInput::new();

        assert_eq!(
            render(
                None,
                &NotRequired("".to_string()),
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None
            ),
            r#"<input type="text" placeholder="Enter some text…" style="left:0px;top:0px;width:100%;height:100%;text-align:center;">"#,
        );
    }

    #[test]
    fn render_text_input_with_default_value() {
        let component = TextInput::new();

        assert_eq!(
            render(
                None,
                &NotRequired("Hello default".to_string()),
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None
            ),
            r#"<input type="text" placeholder="Enter some text…" style="left:0px;top:0px;width:100%;height:100%;text-align:center;" value="Hello default">"#,
        );
    }

    #[test]
    fn render_text_input_required() {
        let component = TextInput::new();

        assert_eq!(
            render(
                None,
                &Required,
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None
            ),
            r#"<input type="text" placeholder="Enter some text…" style="left:0px;top:0px;width:100%;height:100%;text-align:center;" required>"#,
        );
    }

    #[test]
    fn render_text_input_with_name() {
        let component = TextInput::new();

        assert_eq!(
            render(
                Some("my-input"),
                &NotRequired("".to_string()),
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None
            ),
            r#"<input name="my-input" type="text" placeholder="Enter some text…" style="left:0px;top:0px;width:100%;height:100%;text-align:center;">"#,
        );
    }

    #[test]
    fn render_text_input_with_placeholder() {
        let component = TextInput::new().placeholder(Text::new("Enter your country…").color(Red));

        assert_eq!(
            render(
                None,
                &NotRequired("".to_string()),
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None
            ),
            r#"<input type="text" placeholder="Enter your country…" style="left:0px;top:0px;width:100%;height:100%;text-align:center;color:rgb(255,65,54);">"#,
        );
    }

    #[test]
    fn render_text_input_with_min_len() {
        let component = TextInput::new().min_len(10);

        assert_eq!(
            render(
                None,
                &NotRequired("".to_string()),
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None
            ),
            r#"<input type="text" placeholder="Enter some text…" minlength="10" style="left:0px;top:0px;width:100%;height:100%;text-align:center;">"#,
        );
    }

    #[test]
    fn render_text_input_with_max_len() {
        let component = TextInput::new().max_len(20);

        assert_eq!(
            render(
                None,
                &NotRequired("".to_string()),
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None
            ),
            r#"<input type="text" placeholder="Enter some text…" maxlength="20" style="left:0px;top:0px;width:100%;height:100%;text-align:center;">"#,
        );
    }

    #[test]
    fn render_text_input_with_validation() {
        let component = TextInput::new().validation(r#"([A-D])\w+"#);

        assert_eq!(
            render(
                None,
                &NotRequired("".to_string()),
                &component,
                "left:0px;top:0px;width:100%;height:100%;",
                None
            ),
            r#"<input type="text" placeholder="Enter some text…" pattern="([A-D])\w+" style="left:0px;top:0px;width:100%;height:100%;text-align:center;">"#,
        );
    }
}
