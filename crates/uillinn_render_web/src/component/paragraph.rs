use super::{empty_if_default, format_id, sanitize_string};
use uillinn_core::prelude::*;

pub fn render(def: &Paragraph, layout: &str, id: Option<String>) -> String {
    format!(
        r#"<p{} style="{}{}{}{}{}{}{}{}{}">{}</p>"#,
        format_id(&id),
        layout,
        if def.break_word {
            "word-break:break-all;"
        } else {
            ""
        },
        empty_if_default(def.align, Left, "text-align:{};"),
        empty_if_default(def.color, Black, "color:{};"),
        empty_if_default(&def.font_family, &"serif".into(), "font-family:{};"),
        empty_if_default(def.font_size, 16, "font-size:{}px;"),
        empty_if_default(def.font_weight, 400, "font-weight:{};"),
        empty_if_default(def.line_height as f32 / 100., 1.2, "line-height:{};"),
        empty_if_default(def.rotation, 0, "transform:rotate({}deg);"),
        sanitize_string(&def.content),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_paragraph() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.");

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_default_paragraph_sanitized() {
        let component = Paragraph::new(
            r#"<script>alert("Hello world!")</script>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien."#,
        );

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;">&lt;script&gt;alert(&quot;Hello world!&quot;)&lt;/script&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_paragraph_with_color() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.")
            .color(Rgb(51, 51, 51));

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;color:rgb(51,51,51);">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_paragraph_with_font_family() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.")
            .font_family(vec!["system-ui", "sans-serif"]);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;font-family:system-ui,sans-serif;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_paragraph_with_font_size() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.")
            .font_size(20);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;font-size:20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_paragraph_with_font_weight() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.")
            .font_weight(900);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;font-weight:900;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_paragraph_with_alignment() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.").align(Alignment::Center);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;text-align:center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_paragraph_with_break_word() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.").break_word();

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;word-break:break-all;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_paragraph_with_rotation() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.").rotate(-20);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;transform:rotate(-20deg);">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }

    #[test]
    fn render_paragraph_with_line_height() {
        let component = Paragraph::new("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.").line_height(150);

        assert_eq!(
            render(&component, "left:0px;top:0px;width:100%;height:100%;", None),
            r#"<p style="left:0px;top:0px;width:100%;height:100%;line-height:1.5;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia erat mauris, et sollicitudin dolor egestas et. Suspendisse potenti. Suspendisse potenti. Sed semper ultrices orci. Nunc quis nisl ac turpis facilisis aliquet. Phasellus vehicula dolor a nulla posuere, eget iaculis ante interdum. Mauris cursus, lacus vehicula bibendum consectetur, nunc mi fermentum nulla, a aliquet augue sem eu magna. Etiam condimentum augue ipsum, nec luctus mi cursus in. Pellentesque condimentum, dolor ut volutpat pharetra, ipsum risus maximus nunc, ullamcorper vulputate nulla erat in sapien.</p>"#,
        );
    }
}
