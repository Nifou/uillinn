use font_kit::{metrics::Metrics, outline::OutlineSink};
use pathfinder_geometry::{line_segment::LineSegment2F, vector::Vector2F};

pub struct PathOutlineBuilder {
    path: String,
    offset_x: f32,
    offset_y: f32,
    pub scale: f32,
    metrics: Metrics,
}

impl PathOutlineBuilder {
    pub fn new(offset_x: f32, offset_y: f32, font_size: f32, metrics: Metrics) -> Self {
        Self {
            path: String::new(),
            offset_x,
            offset_y,
            scale: font_size / metrics.units_per_em as f32,
            metrics,
        }
    }

    pub fn advance_by(&mut self, val: f32) {
        self.offset_x += val * self.scale;
    }

    pub fn path_width(&self) -> f32 {
        self.offset_x
    }

    pub fn new_line(&mut self, line_height: f32) {
        self.offset_x = 0.;
        self.offset_y += line_height;
    }

    pub fn path_height(&self) -> f32 {
        (self.metrics.ascent - self.metrics.descent) * self.scale
    }

    pub fn get_path_data(self) -> String {
        self.path
    }
}

// grcov-ignore-start
impl OutlineSink for PathOutlineBuilder {
    fn move_to(&mut self, to: Vector2F) {
        self.path.push_str(
            format!(
                "M{} {}",
                to.x() * self.scale + self.offset_x,
                (self.metrics.ascent - to.y()) * self.scale + self.offset_y
            )
            .as_str(),
        );
    }

    fn line_to(&mut self, to: Vector2F) {
        self.path.push_str(
            format!(
                "L{} {}",
                to.x() * self.scale + self.offset_x,
                (self.metrics.ascent - to.y()) * self.scale + self.offset_y
            )
            .as_str(),
        );
    }

    fn quadratic_curve_to(&mut self, ctrl: Vector2F, to: Vector2F) {
        self.path.push_str(
            format!(
                "Q{} {} {} {}",
                ctrl.x() * self.scale + self.offset_x,
                (self.metrics.ascent - ctrl.y()) * self.scale + self.offset_y,
                to.x() * self.scale + self.offset_x,
                (self.metrics.ascent - to.y()) * self.scale + self.offset_y
            )
            .as_str(),
        );
    }

    fn cubic_curve_to(&mut self, ctrl: LineSegment2F, to: Vector2F) {
        self.path.push_str(
            format!(
                "C{} {} {} {} {} {}",
                ctrl.min_x() * self.scale + self.offset_x,
                (self.metrics.ascent - ctrl.min_y()) * self.scale + self.offset_y,
                ctrl.max_x() * self.scale + self.offset_x,
                (self.metrics.ascent - ctrl.max_y()) * self.scale + self.offset_y,
                to.x() * self.scale + self.offset_x,
                (self.metrics.ascent - to.y()) * self.scale + self.offset_y,
            )
            .as_str(),
        );
    }

    fn close(&mut self) {
        self.path.push('Z');
    }
}
// grcov-ignore-stop
