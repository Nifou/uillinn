use super::empty_if_default;
use crate::layout::ComputedArea;
use uillinn_core::prelude::*;

pub fn render(def: &Path, layout: &ComputedArea) -> String {
    format!(
        r#"<svg x="{}" y="{}" width="{}" height="{}" viewBox="0 0 {} {}" preserveAspectRatio="none"><path d="{}" vector-effect="non-scaling-stroke"{}{}{} /></svg>"#,
        layout.x,
        layout.y,
        layout.width,
        layout.height,
        def.bounding_box.0,
        def.bounding_box.1,
        def.to_svg_path(),
        empty_if_default(def.fill_color, Black, r#" fill="{}""#),
        empty_if_default(def.stroke_color, Transparent, r#" stroke="{}""#),
        empty_if_default(def.stroke_size, 1, r#" stroke-width="{}""#),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;
    use uillinn_core::component::CustomComponent;

    #[test]
    fn render_default_path() {
        let component = Path::new((20, 20))
            .move_by(20., 0.)
            .line_by(0., 20.)
            .line_by(-20., 0.)
            .line_by(20., -20.);

        let built_component = if let ComponentKind::Path(path) = component.build().kind {
            path
        } else {
            unreachable!();
        };

        assert_eq!(
            render(
                &built_component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" viewBox="0 0 20 20" preserveAspectRatio="none"><path d="m20,0 l0,20 l-20,0 l20,-20" vector-effect="non-scaling-stroke" stroke="rgb(0,0,0)" stroke-width="0" /></svg>"#,
        );
    }

    #[test]
    fn render_path_with_fill_color() {
        let component = Path::new((20, 20))
            .move_by(20., 0.)
            .line_by(0., 20.)
            .line_by(-20., 0.)
            .line_by(20., -20.)
            .fill_color(Red)
            .close();

        let built_component = if let ComponentKind::Path(path) = component.build().kind {
            path
        } else {
            unreachable!();
        };

        assert_eq!(
            render(
                &built_component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" viewBox="0 0 20 20" preserveAspectRatio="none"><path d="m20,0 l0,20 l-20,0 l20,-20 z" vector-effect="non-scaling-stroke" fill="rgb(255,65,54)" stroke="rgb(0,0,0)" stroke-width="0" /></svg>"#,
        );
    }

    #[test]
    fn render_path_with_stroke_color() {
        let component = Path::new((20, 20))
            .move_by(20., 0.)
            .line_by(0., 20.)
            .line_by(-20., 0.)
            .line_by(20., -20.)
            .stroke_color(Red)
            .close();

        let built_component = if let ComponentKind::Path(path) = component.build().kind {
            path
        } else {
            unreachable!();
        };

        assert_eq!(
            render(
                &built_component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            vec![r#"<svg x="0" y="0" width="100" height="100" viewBox="0 0 20 20" preserveAspectRatio="none"><path d="m20,0 l0,20 l-20,0 l20,-20 z" vector-effect="non-scaling-stroke" stroke="rgb(255,65,54)" stroke-width="0" /></svg>"#,]
                .join("")
        );
    }

    #[test]
    fn render_path_with_stroke_size() {
        let component = Path::new((20, 20))
            .move_by(20., 0.)
            .line_by(0., 20.)
            .line_by(-20., 0.)
            .line_by(20., -20.)
            .stroke_color(Blue)
            .stroke_size(3)
            .close();

        let built_component = if let ComponentKind::Path(path) = component.build().kind {
            path
        } else {
            unreachable!();
        };

        assert_eq!(
            render(
                &built_component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
            ),
            r#"<svg x="0" y="0" width="100" height="100" viewBox="0 0 20 20" preserveAspectRatio="none"><path d="m20,0 l0,20 l-20,0 l20,-20 z" vector-effect="non-scaling-stroke" stroke="rgb(0,116,217)" stroke-width="3" /></svg>"#,
        );
    }
}
