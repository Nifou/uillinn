use crate::layout::ComputedArea;
use uillinn_core::prelude::*;

pub fn render(
    def: &BooleanInput,
    layout: &ComputedArea,
    _overflow: &str,
    image_size: &(usize, usize),
) -> String {
    let case = if def.is_checked_by_default {
        &def.true_case
    } else {
        &def.false_case
    };

    crate::render_to_string_parent(image_size, Some(layout), &Area::new(0, 0, FULL, FULL), case)
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn render_default_boolean_input() {
        let component =
            BooleanInput::new(Rectangle::new().color(Blue), Rectangle::new().color(Red));

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
                "hidden",
                &(1280, 900),
            ),
            crate::render_to_string_parent(
                &(1280, 900),
                Some(&ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                }),
                &Area::new(0, 0, 100, 100),
                &Rectangle::new().color(Red).build()
            )
        );
    }

    #[test]
    fn render_boolean_input_checked_by_default() {
        let component =
            BooleanInput::new(Rectangle::new().color(Blue), Rectangle::new().color(Red))
                .checked_by_default();

        assert_eq!(
            render(
                &component,
                &ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                },
                "hidden",
                &(1280, 900),
            ),
            crate::render_to_string_parent(
                &(1280, 900),
                Some(&ComputedArea {
                    x: 0.,
                    y: 0.,
                    width: 100.,
                    height: 100.
                }),
                &Area::new(0, 0, 100, 100),
                &Rectangle::new().color(Blue).build()
            )
        );
    }
}
